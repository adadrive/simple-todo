package com.ada.todo.web;

import java.io.Serializable;

public class AddListDTO implements Serializable {

    private static final long serialVersionUID = -2882989011684585924L;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
