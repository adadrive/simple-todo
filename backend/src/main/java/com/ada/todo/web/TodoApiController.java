package com.ada.todo.web;

import com.ada.todo.model.TodoList;
import com.ada.todo.service.TodoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4300") // allow requests from the front-end app in local development
public class TodoApiController {

    private final TodoListService todoListService;

    @Autowired
    public TodoApiController(TodoListService todoListService) {
        this.todoListService = todoListService;
    }

    @RequestMapping("/lists")
    public List<TodoList> getAllLists() {
        return todoListService.getAllLists();
    }

    @RequestMapping("/lists/{id}")
    public TodoList getListById(@PathVariable Long id) {
        return todoListService.findListById(id);
    }

    @RequestMapping(value = "/lists", method = RequestMethod.POST)
    public TodoList addList(@RequestBody AddListDTO dto) {
        return todoListService.addNewList(dto.getName());
    }

    @RequestMapping(value = "/lists/{id}", method = RequestMethod.DELETE)
    public void removeListById(@PathVariable Long id) {
        todoListService.deleteList(id);
    }

    // TODO: API's for modifying card details
}

