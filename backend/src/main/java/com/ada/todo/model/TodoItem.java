package com.ada.todo.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class TodoItem implements Serializable {

    private static final long serialVersionUID = -8770816165034546473L;

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @ManyToOne
    private TodoList list;

    protected TodoItem() {
    }

    public TodoItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TodoList getList() {
        return list;
    }

    public void setList(TodoList list) {
        this.list = list;
    }
}
