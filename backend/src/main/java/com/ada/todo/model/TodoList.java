package com.ada.todo.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class TodoList implements Serializable  {

    private static final long serialVersionUID = 2896665683544300337L;

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @Column
    private LocalDateTime created;

    @Column
    private LocalDateTime updated;

    @OneToMany
    private List<TodoItem> items;

    protected TodoList() {
    }

    public TodoList(String name) {
        this.created = LocalDateTime.now();
        this.updated = this.created;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }
}
