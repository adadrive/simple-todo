package com.ada.todo.service;

import com.ada.todo.dao.TodoListDAO;
import com.ada.todo.model.TodoList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Service for managing users lists
 */

@Service
@Transactional
public class TodoListService {

    private final TodoListDAO todoListDAO;

    @Autowired
    public TodoListService(TodoListDAO todoListDAO) {
        this.todoListDAO = todoListDAO;
    }

    public List<TodoList> getAllLists() {
        return todoListDAO.findAll();
    }

    public TodoList findListById(Long id) {
        return todoListDAO.findById(id).orElseThrow(() -> new RuntimeException(String.format("list with id %d not found", id)));
    }

    public TodoList addNewList(String name) {
        return todoListDAO.save(new TodoList(name));
    }

    public void deleteList(Long id) {
        todoListDAO.delete(this.findListById(id));
    }
}
