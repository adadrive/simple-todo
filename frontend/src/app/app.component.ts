import {Component} from '@angular/core';
import {TodoList} from './todolist';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  selectedList: TodoList;

  constructor() {}

  onListSelect(data: TodoList) {
    this.selectedList = data;
  }
}
