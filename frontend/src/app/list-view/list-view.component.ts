import {Component, Input, OnInit} from '@angular/core';
import {TodoList} from '../todolist';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.css']
})
export class ListViewComponent implements OnInit {

   todoList: TodoList;

  constructor() { }

  ngOnInit() {
  }

  @Input()
  set list(todoList: TodoList) {
    this.todoList = todoList;
  }

  getListName(): string {
    return this.todoList !== undefined ? this.todoList.name : '';
  }
}
