import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TodoList} from './todolist';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {}

  getLists(): Observable<TodoList[]> {
    return this.http.get<TodoList[]>('http://localhost:8080/lists');
  }
  getListById(id: number): Observable<TodoList> {
    return this.http.get<TodoList>('http://localhost:8080/lists/' + id);
  }
  addList(name: string): Observable<TodoList> {
    return this.http.post<TodoList>('http://localhost:8080/lists', {name: name});
  }
  removeList(id: number) {
    return this.http.delete('http://localhost:8080/lists/' + id);
  }
}
