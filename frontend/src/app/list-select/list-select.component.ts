import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TodoList} from '../todolist';
import {ApiService} from '../api.service';

@Component({
  selector: 'app-list-select',
  templateUrl: './list-select.component.html',
  styleUrls: ['./list-select.component.css']
})
export class ListSelectComponent implements OnInit {

  private apiService: ApiService;
  private todoList: TodoList;
  lists: TodoList[] = [];
  name: string;

  @Output() listSelected = new EventEmitter<TodoList>();

  constructor(apiService: ApiService) {
    this.apiService = apiService;
  }

  ngOnInit() {
    this.apiService.getLists().subscribe(data => {this.lists = data; });
  }

  onListClick(todoList: TodoList) {
    this.todoList = todoList;
    this.listSelected.emit(todoList);
  }

  addList() {
    this.apiService.addList(this.name).subscribe(list => this.lists.push(list));
    this.name = '';
  }

  removeList(list: TodoList) {
    this.apiService.removeList(list.id).subscribe(() => {
      this.lists = this.lists.filter(i => i.id !== list.id);
    });
  }
}
