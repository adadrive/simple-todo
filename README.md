# simple-todo-app

Base structure for "todo-list" management application

Application consists of _backend_ that provides a simple rest api for updating lists and items. It's built using maven, spring boot and h2 in memory database. Front-end is an angular 5 template project created with _angular-cli_

## before start

node.js with npm and java are required for application to start

From command line

```
node --version // should return something
java --version // should return something
```

## spec

- Header shows the application title
- Todo list selection is in the left side of the screen
- Todo list view is opened to the right side of the screen when user clicks the list name
- Todo list can be removed by clicking icon/button next to the name
- Todo list view lists all items inside the list
- User can add and remove todo lists
- User can change the visible list by selecting it from the list select component
- User can add, remove, modify and mark items completed/uncompleted inside single list
- User can sort items in descending or ascending order within single list
- Todo lists are sorted by creation date (newest as top)
- If user has zero lists show text "No todo lists"

## backend 

Provides a rest api for required functionality. Service uses Spring Boot and Spring Web MVC for content negotiation and actual api implementation. Persistent entities are managed by Spring Data JPA / Hibernate with in-memory database. Because of the in-memory db lists/items are removed when application is stopped

- https://start.spring.io/ maven project using Spring Boot 2.x.x with Web and Data JPA dependencies

```
GET /lists                        // returns all lists
GET /lists/<id>                   // returns single list
POST /lists                       // add new list
DELETE /lists/<id>                // remove list

GET /lists/<id>/items             // listan tehtävät
... add remaining
```

Running the service

```
./mvnw spring-boot:run               // http://localhost:8080/lists should return empty array 
```

## front-end

Provides the front-end for the todo application. Project is generated with __angular-cli__  

```
npm install 
ng serve                // app listens in localhost:4200 by default
```

Creation from scratch 

```
npm install -g @angular/cli
ng new frontend
cd frontend
ng serve

ng generate component todo-list-select
ng generate component todo-list-view
```